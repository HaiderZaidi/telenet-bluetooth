import { ByteUtils } from "../byte-utils";


const GATT_SERVICE_TAIDOC = "00001523-1212-efde-1523-785feabcd123"
const GATT_CHAR_TAIDOC = "00001524-1212-efde-1523-785feabcd123"
const GLUCOSE_GATT_SERVICE_ID = "00001808-0000-1000-8000-00805f9b34fb"

const CMD_GET_DATA = "512600000100a31b"
const CMD_OFF = "515000000000a344"

/**
 * Tested with TD-4279
 */
export class BloodGlucoseTaidoc {
    static deviceData = {
        service: GLUCOSE_GATT_SERVICE_ID,
        name: "TAIDOC TD4279"
    }

    measure(btManager, id, timeoutSec = 15) {
        return new Promise((resolve, reject) => {
            // if (services == null) {
            //     return reject("Failed to discover services!")
            // }

            let timer = setTimeout(() => {
                // this.subscription.remove()
                reject("Measurement timeout!")
            }, + timeoutSec * 1000)

            // let taidocChar = services[GATT_SERVICE_TAIDOC].characteristicsMap[GATT_CHAR_TAIDOC].characteristic;

            // this.subscription = taidocChar.monitor((error, char) => {
            //     // only a single read is expected
            //     clearTimeout(timer)
            //     this.subscription.remove()

            //     if (char != null) {
            //         this.process(resolve, reject, char.value)
            //     } else {
            //         console.log("Read error: " + error.message);
            //         reject(error)
            //     }
            // })


            btManager.setupNotifications(id, GATT_SERVICE_TAIDOC, GATT_CHAR_TAIDOC, (data) => {
                // only a single read is expected
                clearTimeout(timer)
                this.goToSleep(btManager, id)

                console.log("data: " + data)
                this.process(resolve, reject, data)
            })


            setTimeout(() => {
                btManager.write(id, GATT_SERVICE_TAIDOC, GATT_CHAR_TAIDOC, ByteUtils.hexToArray(CMD_GET_DATA))
            }, 2000)

            // // command read last measurement
            // taidocChar.writeWithResponse(ByteUtils.hexToBase64(CMD_GET_DATA)).catch((error) => {
            //     console.log("Write error: " + error.message);
            //     this.subscription.remove()
            //     reject(error)
            // });

        })
    }

    process(resolve, reject, b64) {
        let value = ByteUtils.getIntFromB64(b64, 2, 4, true)
        let falgs = ByteUtils.getIntFromB64(b64, 4, 6, true)

        // see TICD_BGMeter_HM_V1.12_20170822.pdf page 10-11
        let type2 = ((falgs >> 10) & 0b001111)
        let ketone = (type2 == 7)

        let glucometerResult = {}
        if (ketone) {
            glucometerResult['type'] = 'ketone'
            glucometerResult['value'] = value / 30
            glucometerResult['unit'] = 'mmol/L'
        } else {
            glucometerResult['type'] = 'glucose'
            glucometerResult['value'] = value
            glucometerResult['unit'] = 'mg/dL'
        }
        // glucometerResult['date'] = Date.now()

        resolve(glucometerResult)
    }

    goToSleep(btManager, id) {
        btManager.write(id, GATT_SERVICE_TAIDOC, GATT_CHAR_TAIDOC, ByteUtils.hexToArray(CMD_OFF))
        console.log("CMD_OFF sent")
    }
}
