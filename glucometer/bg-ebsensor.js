import { ByteUtils } from "../byte-utils";


const EBSENSOR_GATT_SERVICE_ID = "0000fc00-0000-1000-8000-00805f9b34fb".toUpperCase()
const EBSENSOR_GATT_SERVICE_SHORT_ID = "fc00".toUpperCase()
const EBSENSOR_GATT_CHARACTERISTIC_ID_READ = "0000fca1-0000-1000-8000-00805f9b34fb".toUpperCase()
const EBSENSOR_GATT_CHARACTERISTIC_SHORT_ID_READ = "fca1".toUpperCase()
const EBSENSOR_GATT_CHARACTERISTIC_ID_WRITE = "0000fca0-0000-1000-8000-00805f9b34fb".toUpperCase()
const EBSENSOR_GATT_CHARACTERISTIC_SHORT_ID_WRITE = "fca0".toUpperCase()

const EBSENSOR_GET_DATA = "FA030001AA"


export class BloodGlucoseEbsensor {
    static deviceData = {
        service: EBSENSOR_GATT_SERVICE_SHORT_ID,
        name: "ClinkBlood"
    }

    measure(btManager, id,  timeoutSec = 15) {
        return new Promise((resolve, reject) => {
            // if (services == null) {
            //     return reject("Failed to discover services!")
            // }

            console.log("Starting measurement timeout: " + timeoutSec * 1000)
            let timer = setTimeout(() => {
                reject("Measurement timeout!")
            }, + timeoutSec * 1000)

            // var ebService = services[EBSENSOR_GATT_SERVICE_ID]
            // var readChar = ebService.characteristicsMap[EBSENSOR_GATT_CHARACTERISTIC_ID_READ].characteristic
            // var writeChar = ebService.characteristicsMap[EBSENSOR_GATT_CHARACTERISTIC_ID_WRITE].characteristic

            // readChar.monitor((error, char) => {
            //     if (char != null) {
            //         clearTimeout(timer)
            //         this.process(resolve, reject, char.value)
            //     } else {
            //         console.log("Read error " + error);
            //         reject(error)
            //     }
            // })


            btManager.setupNotifications(id, EBSENSOR_GATT_SERVICE_SHORT_ID, EBSENSOR_GATT_CHARACTERISTIC_SHORT_ID_READ, (data) => {
                console.log("data: " + data)
                this.process(resolve, reject, data)

            })


            setTimeout(() => {
                btManager.write(id, EBSENSOR_GATT_SERVICE_SHORT_ID, EBSENSOR_GATT_CHARACTERISTIC_SHORT_ID_WRITE, ByteUtils.hexToArray(EBSENSOR_GET_DATA))
            }, 2000)

            // // get the latest result
            // // 0xfa, 0x03, 0x00, 0x01, 0xaa
            // writeChar.writeWithoutResponse(ByteUtils.hexToBase64('FA030001AA'))
        })
    }

    process(resolve, reject, b64) {
        let hex = ByteUtils.base64ToHex(b64)
        if (hex.startsWith("fe6a")) {
            reject("Bluetooth disconnected!")
            return
        }
        if (hex.length < 20) {
            // not data package
            console.log('data ' + ByteUtils.base64ToHex(hex))
            return
        }


        let year = ByteUtils.getIntFromB64(b64, 2, 3, true)
        let month = ByteUtils.getIntFromB64(b64, 3, 4, true)
        let day = ByteUtils.getIntFromB64(b64, 4, 5, true)
        let hours = ByteUtils.getIntFromB64(b64, 5, 6, true)
        let minutes = ByteUtils.getIntFromB64(b64, 6, 7, true)

        let data = ByteUtils.getIntFromB64(b64, 8, 10, false)

        let date = new Date(year, month - 1, day, hours, minutes, 0);

        let glucometerResult = {}
        glucometerResult['type'] = 'glucose'
        glucometerResult['value'] = data
        glucometerResult['unit'] = 'mg/dL'
        glucometerResult['date'] = date

        resolve(glucometerResult)
    }


}