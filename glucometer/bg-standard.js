import { ByteUtils } from "../byte-utils";


// see https://www.bluetooth.com/specifications/gatt/viewer?attributeXmlFile=org.bluetooth.service.glucose.xml
const GLUCOSE_GATT_SERVICE_ID = "00001808-0000-1000-8000-00805f9b34fb"
const GLUCOSE_GATT_CHARACTERISTIC_ID_GLUCOSE = "00002a18-0000-1000-8000-00805f9b34fb"
const GLUCOSE_GATT_CHARACTERISTIC_ID_RACP = "00002a52-0000-1000-8000-00805f9b34fb"

// see https://www.bluetooth.com/specifications/gatt/viewer?attributeXmlFile=org.bluetooth.characteristic.record_access_control_point.xml
// 0x01 == Report Stored records
// 0x06 == Last record, 0x05 == First record
const GLUCOSE_RACP_LAST_MEASUREMENT = "0106" // most recent for True Metrix
const GLUCOSE_RACP_FIRST_MEASUREMENT = "0105" // most recent for Taidoc

/** 
 * Tested with True Metrix Air
 */
export class BloodGlucoseStandard {
    static deviceData = {
        service: GLUCOSE_GATT_SERVICE_ID,
        name: "NiproBGM"
    }

    measure(btManager, id, timeoutSec = 15) {
        return new Promise((resolve, reject) => {
            // if (services == null) {
            //     return reject("Failed to discover services!")
            // }

            console.log("Starting measurement timeout: " + timeoutSec * 1000)
            let timer = setTimeout(() => {
                // this.bgSubscription.remove()
                // this.racpSubscription.remove()
                reject("Measurement timeout!")
            }, + timeoutSec * 1000)

            // let gattService = services[GLUCOSE_GATT_SERVICE_ID];
            // let bgChar = gattService.characteristicsMap[GLUCOSE_GATT_CHARACTERISTIC_ID_GLUCOSE].characteristic;
            // let racpChar = gattService.characteristicsMap[GLUCOSE_GATT_CHARACTERISTIC_ID_RACP].characteristic;

            // this.bgSubscription = bgChar.monitor((error, char) => {
            //     if (char != null) {
            //         clearTimeout(timer)
            //         console.log("bg " + ByteUtils.base64ToHex(char.value));
            //         this.process(resolve, reject, char.value)
            //     } else {
            //         console.log("Read error: " + error.message);
            //         reject(error)
            //     }
            //     this.bgSubscription.remove()
            // })

            // this.racpSubscription = racpChar.monitor((error, char) => {
            //     if (char != null) {
            //         console.log("racp " + ByteUtils.base64ToHex(char.value));
            //     } else {
            //         console.log("Read error: " + error.message);
            //         reject(error.message)
            //     }
            //     this.racpSubscription.remove()
            // })

            btManager.setupNotifications(id, GLUCOSE_GATT_SERVICE_ID, GLUCOSE_GATT_CHARACTERISTIC_ID_GLUCOSE, (data) => {
                console.log("data: " + data)
                const hex = ByteUtils.base64ToHex(data)
                this.process(resolve, reject, hex)

            })


            setTimeout(() => {
                btManager.write(id, GLUCOSE_GATT_SERVICE_ID, GLUCOSE_GATT_CHARACTERISTIC_ID_GLUCOSE, ByteUtils.hexToArray(GLUCOSE_RACP_LAST_MEASUREMENT))
            }, 2000)

            // racpChar.writeWithResponse(ByteUtils.hexToBase64(GLUCOSE_RACP_LAST_MEASUREMENT)).catch((error) => {
            //     console.log("Write error: " + error.message);
            //     this.bgSubscription.remove()
            //     this.racpSubscription.remove()
            //     reject(reject.message)
            // });

        })
    }

    process(resolve, reject, b64) {
        let flags = ByteUtils.getIntFromB64(b64, 0, 1, true)
        // let seq = ByteUtils.getIntFromB64(b64, 1, 3, true)

        let year = ByteUtils.getIntFromB64(b64, 3, 5, true)
        let month = ByteUtils.getIntFromB64(b64, 5, 6, true)
        let day = ByteUtils.getIntFromB64(b64, 6, 7, true)
        let hours = ByteUtils.getIntFromB64(b64, 7, 8, true)
        let minutes = ByteUtils.getIntFromB64(b64, 8, 9, true)
        // let sec = ByteUtils.getIntFromB64(b64, 9, 10, true)

        let data = ByteUtils.getIntFromB64(b64, 10, 12, true)

        let mol = (flags & 0b00000100) > 0;
        let value = ByteUtils.sfloatToNumber(data)
        let date = new Date(year, month - 1, day, hours, minutes, 0);

        if (mol) {
            // mol/L --> mmol/L
            value *= 1000.0;
        } else {
            // kg/L --> mg/dL
            value *= 1000.0 * 1000.0 / 10.0;
        }

        let glucometerResult = {}
        glucometerResult['type'] = 'glucose'
        glucometerResult['value'] = value
        if (mol) {
            glucometerResult['unit'] = 'mmol/L'
        } else {
            glucometerResult['unit'] = 'mg/dL'
        }
        glucometerResult['date'] = date

        resolve(glucometerResult)
    }

}