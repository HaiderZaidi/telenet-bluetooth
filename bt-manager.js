import BleManager from 'react-native-ble-manager';
import { ByteUtils } from "./byte-utils";
import {
    NativeEventEmitter,
    NativeModules,
} from 'react-native';

const BleManagerModule = NativeModules.BleManager;
const bleManagerEmitter = new NativeEventEmitter(BleManagerModule);

export class BtManager {
    manager
    lastDeviceId

    init() {
        if (this.manager == null) {
            this.manager = BleManager;

            this.state = {
                scanning: false,
                peripherals: new Map(),
                deviceInfo: null,
                scanCallback: null,
                dataCallback: null,
                stateCallback: null,
                connecting: false,
            }

            this.manager.start({ showAlert: false });


            this.handleDiscoverPeripheral = this.handleDiscoverPeripheral.bind(this);
            this.handleStopScan = this.handleStopScan.bind(this);
            this.handleUpdateValueForCharacteristic = this.handleUpdateValueForCharacteristic.bind(this);
            this.handleState = this.handleUpdateState.bind(this);
            // this.handleDisconnectedPeripheral = this.handleDisconnectedPeripheral.bind(this);

            this.handlerDiscover = bleManagerEmitter.addListener('BleManagerDiscoverPeripheral', this.handleDiscoverPeripheral);
            this.handlerStop = bleManagerEmitter.addListener('BleManagerStopScan', this.handleStopScan);
            this.handlerUpdate = bleManagerEmitter.addListener('BleManagerDidUpdateValueForCharacteristic', this.handleUpdateValueForCharacteristic);
            this.handlerUpdateState = bleManagerEmitter.addListener('BleManagerDidUpdateState', this.handleState)
            // this.handlerDisconnect = bleManagerEmitter.addListener('BleManagerDisconnectPeripheral', this.handleDisconnectedPeripheral);
        }
    }

    destroy() {
        if (this.manager != null) {
            this.disconnect()
            this.manager.stopScan()
            this.handlerDiscover.remove();
            this.handlerStop.remove();
            // this.handlerDisconnect.remove();
            this.handlerUpdateState.remove()
            this.handlerUpdate.remove();
        }
    }

    handleDiscoverPeripheral(peripheral) {
        var peripherals = this.state.peripherals;
        if (!peripherals.has(peripheral.id)) {
            if (peripheral.name != null && peripheral.name.startsWith(this.state.deviceInfo.name)) {
                console.log("Device found: " + peripheral.name + " " + peripheral.id)
                this.state.connecting = true
                this.manager.stopScan().then(() => {
                    this.state.scanning = false
                    this.connectToDevice(peripheral.id, [this.state.deviceInfo.service])
                }).catch((error) => {
                    console.log("Stop scan error: " + error)
                })
            }
            peripherals.set(peripheral.id, peripheral);
            this.state.peripherals = peripherals
        }
    }

    handleStopScan() {
        console.log('Scan is stopped');

        this.state.scanning = false

        if (!this.state.connecting) {
            this.state.scanCallback(false, undefined)
        }
    }

    checkState(stateCallback) {
        this.init()
        this.state.stateCallback = stateCallback
        this.manager.checkState()
    }

    async mapServicesAndCharacteristicsForDevice(peripheralId) {
        this.manager.retrieveServices(peripheralId)
            .then((peripheralInfo) => {
                // Success code
                console.log('Peripheral info:', peripheralInfo);
                this.state.scanCallback(true, peripheralId)
            }).catch((error) => {
                console.log('Error: ', error);
                this.state.scanCallback(false, peripheralId)
            })
    }

    async connectToDevice(peripheralId, serviceIds = [], strictConnectionCheck = false) {
        let alredyConnected = await this.manager.isPeripheralConnected(peripheralId, serviceIds)
        if (!alredyConnected) {
            if (this.state.scanning) {
                console.log("Warning: Scanning is still in progress!")
            }

            try {
                await this.manager.connect(peripheralId)
            } catch (error) {
                console.log('Connection error: ' + error)

                this.state.scanCallback(false, peripheralId)
                return
            }

            if (strictConnectionCheck) {
                let connected = await this.manager.isPeripheralConnected(peripheralId, serviceIds)
                if (!connected) {
                    console.log('Failed to connect to device: ' + peripheralId)

                    this.state.scanCallback(false, peripheralId)
                    return
                }
            }
        }

        this.lastDeviceId = peripheralId
        console.log('Connected to device: ' + peripheralId)

        this.mapServicesAndCharacteristicsForDevice(peripheralId)
    }

    async disconnect(deviceId = this.lastDeviceId) {
        this.init()

        this.state.connecting = false
        if (deviceId != null) {
            try {
                await this.manager.disconnect(deviceId, true)
                console.log("Disconnected from " + deviceId)
            } catch (err) {
                console.warn("Failed to disconnect from " + deviceId)
                return false
            }

            if (deviceId == this.lastDeviceId) {
                this.lastDeviceId = null
            }
            return true
        }
        return false
    }


    async scanForDevice(deviceData, timeoutSec = 10, callback) {
        this.init()

        if (!this.state.scanning) {
            this.state.peripherals = new Map()
            this.state.deviceInfo = deviceData
            this.state.scanCallback = callback

            this.manager.scan([deviceData.service], timeoutSec, true).then(() => {
                console.log("Starting scan for: " + deviceData.service)
                this.state.scanning = true
            });
        } else {
            throw 'Already scanning!'
        }
    }

    setupNotifications(deviceId, serviceId, charId, dataCallback) {
        this.state.dataCallback = dataCallback
        this.manager.startNotification(deviceId, serviceId, charId)
            .catch((error) => { throw Error("startNotification error: " + error) })
    }

    cancelNotifications(deviceId, serviceId, charId) {
        this.manager.stopNotification(deviceId, serviceId, charId)
            .catch((error) => { console.log("stopNotification error: " + error) })
    }

    write(deviceId, serviceId, charId, data) {
        this.manager.write(deviceId, serviceId, charId, data, 20)
            .catch(error => console.log("write error: " + error))
    }

    handleUpdateValueForCharacteristic(data) {
        console.log('Received data from ' + data.peripheral + ' characteristic ' + data.characteristic, data.value);
        this.state.dataCallback(ByteUtils.arrayToB64(data.value))
    }

    handleUpdateState(bluetoothState) {
        if (this.state.stateCallback) {
            this.state.stateCallback(bluetoothState.state == "on")
            this.state.stateCallback = null
        }
        console.log(bluetoothState)
    }

}