import { ByteUtils } from "../byte-utils";


const PULSE_OXIMETER_GATT_SERVICE_ID = "cdeacb80-5235-4c07-8846-93a37ee6b86d"
const PULSE_OXIMETER_GATT_SERVICE_CHARACTERISTIC_ID = "cdeacb81-5235-4c07-8846-93a37ee6b86d"

const PULSE_OXIMETER_DUMMY_DATA_PULSE = 255;
const PULSE_OXIMETER_DUMMY_DATA_SPO2 = 127;
const PULSE_OXIMETER_DUMMY_DATA_PI = 0;

const PULSE_OXIMETER_DATA_WAIT_COUNT = 10;
const PULSE_OXIMETER_DATA_AVEREGE_COUNT = 3;

const defaultCallback = (progress) => {
    console.log('Progress ' + progress)
}

export class PulseOximeterJumper {
    static deviceData = {
        service: PULSE_OXIMETER_GATT_SERVICE_ID,
        name: "My Oximeter"
    }

    timerId = 0

    measure(btManager, id, timeoutSec = 20, progressCallback = defaultCallback) {
        return new Promise((resolve, reject) => {

            progressCallback(0.0)
            this.resetTimeout(btManager, id, reject, timeoutSec)

            this.resultList = []

            btManager.setupNotifications(id, PULSE_OXIMETER_GATT_SERVICE_ID, PULSE_OXIMETER_GATT_SERVICE_CHARACTERISTIC_ID, (data) => {
                console.log("data: " + data)
                this.process(btManager, id, resolve, reject, data, timeoutSec, progressCallback)
            })

        })
    }

    process(btManager, id, resolve, reject, b64, timeoutSec, progressCallback) {
        let length = ByteUtils.getLength(b64)
        if (length == 4) {
            // console.log('data ' + ByteUtils.base64ToHex(b64));

            let pulse = ByteUtils.getIntFromB64(b64, 1, 2, false)
            let spo2 = ByteUtils.getIntFromB64(b64, 2, 3, false)
            let pi = ByteUtils.getIntFromB64(b64, 3, 4, false)
            let piPercent = pi / 10.0

            if (pulse == PULSE_OXIMETER_DUMMY_DATA_PULSE
                || spo2 == PULSE_OXIMETER_DUMMY_DATA_SPO2
                || pi == PULSE_OXIMETER_DUMMY_DATA_PI) {
                // dummy data from the sensor -> drop collected data and wait for correct values
                this.resultList = []
                progressCallback(0.0)
                return
            }

            // reset timeout only for real values
            this.resetTimeout(btManager, id, reject, timeoutSec)

            let oximeterResult = {};
            oximeterResult['pulse'] = pulse
            oximeterResult['spo2'] = spo2
            oximeterResult['pi'] = piPercent

            this.resultList.push(oximeterResult)
            console.log("count " + this.resultList.length)

            progressCallback(this.resultList.length / PULSE_OXIMETER_DATA_WAIT_COUNT)
            if (this.resultList.length >= PULSE_OXIMETER_DATA_WAIT_COUNT) {
                clearTimeout(this.timerId)
                btManager.cancelNotifications(id, PULSE_OXIMETER_GATT_SERVICE_ID, PULSE_OXIMETER_GATT_SERVICE_CHARACTERISTIC_ID)
                resolve(this.getAverage(this.resultList));
            }
        }
    }

    getAverage(resultList) {
        let oximeterSum = {};
        oximeterSum['pulse'] = 0
        oximeterSum['spo2'] = 0
        oximeterSum['pi'] = 0.0
        oximeterSum['count'] = 0

        resultList.forEach((value, index) => {
            if (index >= (PULSE_OXIMETER_DATA_WAIT_COUNT - PULSE_OXIMETER_DATA_AVEREGE_COUNT)) {
                oximeterSum['pulse'] += +value['pulse']
                oximeterSum['spo2'] += +value['spo2']
                oximeterSum['pi'] += +value['pi']
                oximeterSum['count']++
            }
        })

        let oximeterAverage = {};
        oximeterAverage['pulse'] = Math.round(oximeterSum['pulse'] / oximeterSum['count'])
        oximeterAverage['spo2'] = Math.round(oximeterSum['spo2'] / oximeterSum['count'])
        oximeterAverage['pi'] = oximeterSum['pi'] / oximeterSum['count']

        return oximeterAverage
    }

    resetTimeout(btManager, id, reject, timeoutSec) {
        clearTimeout(this.timerId)
        this.timerId = setTimeout(() => {
            btManager.cancelNotifications(id, PULSE_OXIMETER_GATT_SERVICE_ID, PULSE_OXIMETER_GATT_SERVICE_CHARACTERISTIC_ID)
            reject("Measurement timeout!")
        }, + timeoutSec * 1000)
    }

}
