import base64 from "base64-js"
import { Buffer } from 'buffer'


const SFLOAT_POSITIVE_INFINITY = 0x07FE;
const SFLOAT_NaN = 0x07FF;
const SFLOAT_NRes = 0x0800;
const SFLOAT_RESERVED_VALUE = 0x0801;
const SFLOAT_NEGATIVE_INFINITY = 0x0802;

export class ByteUtils {

    static arrayToHex(array) {
        return Array.prototype.map.call(array, x => ('00' + x.toString(16)).slice(-2)).join('');
    }

    static arrayToB64(array) {
        return this.hexToBase64(this.arrayToHex(array))
    }

    static stringToHex(str) {
        var bytes = [];
        var buffer = new Buffer(str, 'utf8');
        for (var i = 0; i < buffer.length; i++) {
            bytes.push(buffer[i]);
        }
        return this.arrayToHex(bytes)
    }

    static hexToArray(hex) {
        return Array.from(this.hexToUint8Array(hex))
    }

    static hexToUint8Array(hex) {
        var typedArray = new Uint8Array(hex.match(/[\da-f]{2}/gi).map(function (h) {
            return parseInt(h, 16)
        }))
        return typedArray;
    }

    static hexToBase64(hexStr) {
        if (hexStr == null) {
            throw "hexToBase64 - Invalid argument: hex string is null!"
        }

        return base64.fromByteArray(this.hexToUint8Array(hexStr));
    }

    static base64ToHex(base64Str) {
        if (base64Str == null) {
            throw "base64ToHex - Invalid argument: base64 string is null!"
        }

        return this.arrayToHex(base64.toByteArray(base64Str));
    }

    /**
     * convert an array to a single number
     * @param array Array
     */
    static byteArrayToNumber(array) {
        return array.reduce((previous, current) => {
            return (previous * 0x100) + current
        })
    }

    static byteArrayChecksum(array) {
        return array.reduce((previous, current) => {
            return (previous + current) % 0x10000
        })
    }

    static sfloatToNumber(value) {
        switch (value) {
            case SFLOAT_NaN:
            case SFLOAT_NRes:
            case SFLOAT_RESERVED_VALUE:
                return Number.NaN;
            case SFLOAT_POSITIVE_INFINITY:
                return Number.POSITIVE_INFINITY;
            case SFLOAT_NEGATIVE_INFINITY:
                return Number.NEGATIVE_INFINITY;
            default:
                return this.getMantissa(value) * Math.pow(10.0, this.getExponent(value));
        }
    }

    static getExponent(value) {
        var exoponent = (value >> 12)
        if (exoponent >= 0x0008) { // if exponent should be negative
            exoponent = - ((0x000F + 1) - exoponent);
        }
        return exoponent
    }

    static getMantissa(value) {
        var mantissa = (value & 0x0FFF)
        if (mantissa >= 0x0800) { // if mantissa should be negative
            mantissa = - ((0x0FFF + 1) - mantissa);
        }
        return mantissa;
    }

    static getIntFromHex(hex, from, to, reverse = true) {
        if (hex == null) {
            throw "getIntFromHex - Invalid argument: hex string is null!"
        }

        return this.getIntFromB64(this.hexToBase64(hex), from, to, reverse)
    }

    static getIntFromB64(b64, from, to, reverse = true) {
        if (b64 == null) {
            throw "getIntFromB64 - Invalid argument: base64 string is null!"
        }

        var uint8Array = base64.toByteArray(b64)
        var array = Array.from(uint8Array)
        if (from > to || from > array.length || to > array.length) {
            throw "Invalid index argument! " + from + "-" + to + "/" + array.length
        }
        var part = array.slice(from, to)
        if (reverse) {
            return this.byteArrayToNumber(part.reverse())
        }
        return this.byteArrayToNumber(part)
    }

    static getLength(b64) {
        return base64.byteLength(b64)
    }

    static numberTo1ByteHex(number) {
        if (number > 0xff) {
            console.log("Warning, too large number for 1 bytes: " + number)
        }
        return this.arrayToHex([+number % 0x100])
    }

    static numberTo2ByteHex(number) {
        if (number > 0xffff) {
            console.log("Warning, too large number for 2 bytes: " + number)
        }
        return this.arrayToHex([+number % 0x100, Math.floor(+number / 0x100)])
    }

    static numberTo4ByteHex(number) {
        if (number > 0xffffffff) {
            console.log("Warning, too large number for 4 bytes: " + number)
        }
        return this.numberTo2ByteHex(+number % 0x10000) + this.numberTo2ByteHex(Math.floor(+number / 0x10000))
    }

}