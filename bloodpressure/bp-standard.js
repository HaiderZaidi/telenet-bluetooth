import { ByteUtils } from "../byte-utils";


// see https://www.bluetooth.com/specifications/gatt/viewer?attributeXmlFile=org.bluetooth.service.blood_pressure.xml
const BP_GATT_SERVICE_ID = "00001810-0000-1000-8000-00805f9b34fb"
const BP_GATT_CHARACTERISTIC_ID_BP = "00002a35-0000-1000-8000-00805f9b34fb"


export class BloodPressureStandard {
    static deviceData = {
        service: BP_GATT_SERVICE_ID,
        name: ""
    }

    measure(services, timeoutSec = 15) {
        return new Promise((resolve, reject) => {
            if (services == null) {
                return reject("Failed to discover services!")                
            }

            console.log("Starting measurement timeout: " + timeoutSec * 1000)
            let timer = setTimeout(() => {
                reject("Measurement timeout!")
            }, + timeoutSec * 1000)

            let gattService = services[BP_GATT_SERVICE_ID];
            let readChar = gattService.characteristicsMap[BP_GATT_CHARACTERISTIC_ID_BP].characteristic;

            let subscription = readChar.monitor((error, char) => {
                if (char != null) {
                    clearTimeout(timer)
                    this.process(resolve, reject, char.value)
                    console.log("bp " + ByteUtils.base64ToHex(char.value));
                } else {
                    console.log("error " + error.message);
                }
                subscription.remove()
            })
        })
    }

    process(resolve, reject, buffer) {
        //    TODO implement
    }
}