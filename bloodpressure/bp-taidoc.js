import { ByteUtils } from "../byte-utils";


const BP_GATT_SERVICE_TAIDOC = "00001523-1212-efde-1523-785feabcd123"
const BP_GATT_CHAR_TAIDOC = "00001524-1212-efde-1523-785feabcd123"
const BP_GATT_SERVICE_ID = "00001810-0000-1000-8000-00805f9b34fb"

const START_BYTE = "51"
const STOP_BYTE_COMMAND = "a3"
const STOP_BYTE_DEVICE = "a5"
const CMD_READ_MEASUREMENT_TIME = "25"
const CMD_READ_MEASUREMENT_DATA = "26"
const CMD_OFF = "50"


/**
 * Tested with TD-3128
 */
export class BloodPressureTaidoc {
    static deviceData = {
        service: BP_GATT_SERVICE_ID,
        name: "TAIDOC TD3128"
    }

    measure(btManager, id, timeoutSec = 15) {
        return new Promise((resolve, reject) => {

            this.resetTimeout(reject, timeoutSec)
            this.state = 1
            this.times = []
            this.btManager = btManager
            this.id = id

            btManager.setupNotifications(id, BP_GATT_SERVICE_TAIDOC, BP_GATT_CHAR_TAIDOC, (data) => {
                console.log("data: " + ByteUtils.base64ToHex(data))
                this.process(resolve, reject, data)
            })

            setTimeout(() => {
                this.getTime()
            }, 2000)

        })
    }

    process(resolve, reject, b64) {
        // packet checks
        let hex = ByteUtils.base64ToHex(b64)
        if (!hex.startsWith(START_BYTE) || hex.length != 16) {
            reject("Invalid packet!")
            return
        }

        let checksum = ByteUtils.getIntFromHex(hex, 7, 8, false)
        let calculatedSum = ByteUtils.byteArrayChecksum(ByteUtils.hexToArray(hex.slice(0, hex.length - 1))) % 0x100
        console.log("calculated sum " + calculatedSum)
        if (checksum != calculatedSum) {
            reject("Wrong packet checksum!")
            return
        }

        // valid packet, cancel timeout
        this.resetTimeout(reject, 5)

        let cmd = ByteUtils.getIntFromHex(hex, 1, 2, false)
        if (cmd == ByteUtils.getIntFromHex(CMD_READ_MEASUREMENT_DATA, 0, 1, false)) {
            // got a measurement data, turn off the device and finish processing
            this.goToSleep()
            this.processData(resolve, b64)
        } else if (cmd == ByteUtils.getIntFromHex(CMD_READ_MEASUREMENT_TIME, 0, 1, false)) {
            // got a measurement time
            this.processTime(reject, b64)
        } else {
            reject("Unexpected command!")
        }
    }

    processTime(reject, b64) {
        // see TICD_BPMeter_V1.10_20150812.pdf page 7 Table A
        let date = ByteUtils.getIntFromB64(b64, 2, 4, true)
        // see TICD_BPMeter_V1.10_20150812.pdf page 10 Table C, D
        let timeAndFlags = ByteUtils.getIntFromB64(b64, 4, 6, true)
        let time = timeAndFlags & 0b0001111100111111
        // metric isn't a sensible datetime, but it's good for comparison
        let metric = (date * 0x10000) + time

        console.log("time " + this.state + " is " + metric)
        this.times[this.state] = metric

        if (this.state < 4) {
            // continue with the next user
            this.state += 1
            this.getTime()

        } else {
            console.log("times " + this.times)

            // find the latest measurement
            let max = this.times[1]
            for (let i = 1; i <= 4; i++) {
                if (this.times[i] >= max) {
                    max = this.times[i]
                    this.selectedUser = i
                }
            }
            console.log("max " + max + " selected user " + this.selectedUser)
            // retrieve the latest measuremtn
            this.getData()
        }
    }

    processData(resolve, b64) {
        // see TICD_BPMeter_V1.10_20150812.pdf page 11
        let sys = ByteUtils.getIntFromB64(b64, 2, 3, true)
        let dia = ByteUtils.getIntFromB64(b64, 4, 5, true)
        let pulse = ByteUtils.getIntFromB64(b64, 5, 6, true)

        let bloodPressureResult = {}
        bloodPressureResult['sys'] = sys
        bloodPressureResult['dia'] = dia
        bloodPressureResult['pulse'] = pulse

        resolve(bloodPressureResult)
    }


    getTime() {
        this.btManager.write(this.id, BP_GATT_SERVICE_TAIDOC, BP_GATT_CHAR_TAIDOC, ByteUtils.hexToArray(
            this.createCommandString(CMD_READ_MEASUREMENT_TIME, "000000" + ByteUtils.numberTo1ByteHex(this.state))
        ))
    }

    getData() {
        this.btManager.write(this.id, BP_GATT_SERVICE_TAIDOC, BP_GATT_CHAR_TAIDOC, ByteUtils.hexToArray(
            this.createCommandString(CMD_READ_MEASUREMENT_DATA, "000000" + ByteUtils.numberTo1ByteHex(this.selectedUser))
        ))
    }

    goToSleep() {
        this.btManager.write(this.id, BP_GATT_SERVICE_TAIDOC, BP_GATT_CHAR_TAIDOC, ByteUtils.hexToArray(
            this.createCommandString(CMD_OFF, "00000000")
        ))
        console.log("CMD_OFF sent")
    }

    createCommandString(command, params = '00000000') {
        if (params.length != 8) {
            throw "Commad parameter error!"
        }

        let data = START_BYTE + command + params + STOP_BYTE_COMMAND
        let checksum = ByteUtils.byteArrayChecksum(ByteUtils.hexToArray(data))
        data += ByteUtils.numberTo1ByteHex(checksum)

        return data
    }


    resetTimeout(reject, timeoutSec) {
        clearTimeout(this.timerId)
        this.timerId = setTimeout(() => {
            reject("Measurement timeout!")
        }, + timeoutSec * 1000)
    }

}