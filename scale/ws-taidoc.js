import { ByteUtils } from "../byte-utils";


const GATT_SERVICE_TAIDOC = "00001523-1212-efde-1523-785feabcd123"
const GATT_CHAR_TAIDOC = "00001524-1212-efde-1523-785feabcd123"
const GATT_SERVICE_DEVICE_INFO = "0000180a-0000-1000-8000-00805f9b34fb"

// start, command, length
const HEADER_LENGTH = 3
// stop, checksum
const FOOTER_LENGTH = 2

const START_BYTE = "51"
const STOP_BYTE_COMMAND = "a3"
const STOP_BYTE_DEVICE = "a5"

// start, command, length, index high, index low, stop, checksum
const CMD_GET_LAST_DATA = "5171020000a367"
const CMD_OFF = "515000000000a344"

/**
 * Tested with TD-2555
 */
export class WeightScaleTaidoc {
    static deviceData = {
        service: GATT_SERVICE_DEVICE_INFO,
        name: "TAIDOC TD2555"
    }

    timerId = 0
    recieveBuffer = new String('')

    measure(btManager, id, timeoutSec = 15) {
        return new Promise((resolve, reject) => {
            // if (services == null) {
            //     return reject("Failed to discover services!")
            // }

            this.resetTimeout(reject, timeoutSec)

            // let taidocChar = services[GATT_SERVICE_TAIDOC].characteristicsMap[GATT_CHAR_TAIDOC].characteristic;

            btManager.setupNotifications(id, GATT_SERVICE_TAIDOC, GATT_CHAR_TAIDOC, (data) => {
                console.log("data: " + data)
                const hex = ByteUtils.base64ToHex(data)
                this.process(resolve, reject, hex, btManager, id)

            })


            setTimeout(() => {
                btManager.write(id, GATT_SERVICE_TAIDOC, GATT_CHAR_TAIDOC, ByteUtils.hexToArray(CMD_GET_LAST_DATA))
            }, 2000)
            

            // this.subscription = taidocChar.monitor((error, char) => {
            //     if (char != null) {
            //         this.resetTimeout(reject, timeoutSec)
            //         const hex = ByteUtils.base64ToHex(char.value)
            //         console.log('Recieved ws ' + hex)
            //         this.process(resolve, reject, hex)
            //     } else {
            //         console.log("Read error: " + error.message);
            //         this.subscription.remove()
            //         reject(error)
            //     }
            // })

            // // read last measurement's data
            // taidocChar.writeWithResponse(ByteUtils.hexToBase64(CMD_GET_LAST_DATA)).catch((error) => {
            //     console.log("Write error: " + error.message);
            //     this.subscription.remove()
            //     reject(error)
            // });

        })
    }

    process(resolve, reject, recieved, btManager, id) {
        this.recieveBuffer += recieved
        let next = null

        // length is the 3rd byte
        if (this.recieveBuffer.length >= (3 * 2) && this.recieveBuffer.startsWith(START_BYTE)) {
            const len = ByteUtils.getIntFromHex(this.recieveBuffer, 2, 3)
            const completeLen = (HEADER_LENGTH + len + FOOTER_LENGTH) * 2

            if (this.recieveBuffer.length > completeLen
                && (this.recieveBuffer.lastIndexOf(STOP_BYTE_DEVICE) > 0)) {
                // Longer than expected, try to cut to size
                next = this.recieveBuffer.slice(completeLen, this.recieveBuffer.length)
                this.recieveBuffer = this.recieveBuffer.slice(0, completeLen)
            }

            if (this.recieveBuffer.length == completeLen) {
                // complete packet
                const unit = ByteUtils.getIntFromHex(this.recieveBuffer, 15, 16, false)
                const weightKg = ByteUtils.getIntFromHex(this.recieveBuffer, 16, 18, false)
                const weightLb = ByteUtils.getIntFromHex(this.recieveBuffer, 18, 20, false)
                const bmi = ByteUtils.getIntFromHex(this.recieveBuffer, 20, 22, false)

                let weightResult = {}
                weightResult['value-kg'] = weightKg / 10.0
                weightResult['value-lb'] = weightLb / 10.0
                weightResult['value-bmi'] = bmi / 10.0
                if (unit == 0) {
                    weightResult['unit'] = 'kg'
                } else if (unit == 1) {
                    weightResult['unit'] = 'lb'
                } else {
                    weightResult['unit'] = 'unknown'
                }

                this.recieveBuffer = ""
                clearTimeout(this.timerId)
                this.goToSleep(btManager, id)
                resolve(weightResult)
                return
            } else if (this.recieveBuffer.length < completeLen && this.recieveBuffer.startsWith(START_BYTE)) {
                // incoming packet
                console.log("incoming packet: " + this.recieveBuffer.length + "/" + completeLen)
            } else {
                // unexpected size
                this.recieveBuffer = ""
                reject("Wrong packet size!")
                return
            }

        } else if (!this.recieveBuffer.startsWith(START_BYTE)) {
            console.log("Wrong packet start!")
            this.recieveBuffer = ""
            return
        }

        if (next != null) {
            this.process(resolve, reject, next)
        }
    }

    resetTimeout(reject, timeoutSec) {
        clearTimeout(this.timerId)
        this.timerId = setTimeout(() => {
            // this.subscription.remove()
            reject("Measurement timeout!")
        }, + timeoutSec * 1000)
    }

    goToSleep(btManager, id) {
        btManager.write(id, GATT_SERVICE_TAIDOC, GATT_CHAR_TAIDOC, ByteUtils.hexToArray(CMD_OFF))
        console.log("CMD_OFF sent")
    }
}
