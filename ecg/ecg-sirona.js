import { ByteUtils } from "../byte-utils";
import {
    SUP,
    CommandReceiveChallenge
} from "./sup";


const SIRONA_GATT_SERVICE_ID = "da2b84f1-6279-48de-bdc0-afbea0226079"
const SIRONA_GATT_CHARACTERISTIC_ID_INFO = "99564a02-dc01-4d3c-b04e-3bb1ef0571b2"
const SIRONA_GATT_CHARACTERISTIC_ID_MODE = "a87988b9-694c-479c-900e-95dfa6c00a24"
const SIRONA_GATT_CHARACTERISTIC_ID_RX = "bf03260c-7205-4c25-af43-93b1c299d159"
const SIRONA_GATT_CHARACTERISTIC_ID_TX = "18cda784-4bd3-4370-85bb-bfed91ec86af"
const SIRONA_GATT_CHARACTERISTIC_ID_CTS = "0a1934f5-24b8-4f13-9842-37bb167c6aff"
const SIRONA_GATT_CHARACTERISTIC_ID_RTS = "fdd6b4d3-046d-4330-bdec-1fd0c90cb43b"

const BRSP_MODE_IDLE = 0x00;
const BRSP_MODE_DATA = 0x01;
const BRSP_MODE_COMMAND = 0x02;
const BRSP_MODE_FIRMWARE_UPDATE = 0x04;

const SIRONA_DEV_DECODE_KEY = "ce000000ca000000ca000000fe000000";

const defaultCallback = (progress, data) => {
    console.log('Progress ' + progress + " " + data)
}


export class EcgSirona {
    static deviceData = {
        service: SIRONA_GATT_SERVICE_ID,
        name: "SironaPWM-"
    }

    timerId = 0

    measure(services, timeoutSec = 30, decodeKey = SIRONA_DEV_DECODE_KEY, progressCallback = defaultCallback) {
        return new Promise((resolve, reject) => {
            if (services == null) {
                return reject("Failed to discover services!")
            }

            progressCallback(0.0, null)
            this.resetTimeout(reject, timeoutSec)

            let gattService = services[SIRONA_GATT_SERVICE_ID]
            let readChar = gattService.characteristicsMap[SIRONA_GATT_CHARACTERISTIC_ID_TX].characteristic
            let writeChar = gattService.characteristicsMap[SIRONA_GATT_CHARACTERISTIC_ID_RX].characteristic
            // let ctsChar = gattService.characteristicsMap[SIRONA_GATT_CHARACTERISTIC_ID_CTS].characteristic
            // let rtsChar = gattService.characteristicsMap[SIRONA_GATT_CHARACTERISTIC_ID_RTS].characteristic
            // let infoChar = gattService.characteristicsMap[SIRONA_GATT_CHARACTERISTIC_ID_INFO].characteristic
            let modeChar = gattService.characteristicsMap[SIRONA_GATT_CHARACTERISTIC_ID_MODE].characteristic

            this.sup = new SUP(decodeKey, writeChar)

            this.subscription = readChar.monitor((error, char) => {
                if (char != null) {
                    this.resetTimeout(reject, timeoutSec)
                    if (char.value == null) {
                        console.log("Read witout a value!")
                        return
                    }
                    const hex = ByteUtils.base64ToHex(char.value)
                    console.log('Recieved ' + hex)
                    this.sup.parseRecievedData(resolve, reject, hex, progressCallback)
                } else {
                    console.log("Read error: " + error.message);
                    this.subscription.remove()
                    reject(error)
                }
            })

            // rtsChar.monitor((error, char) => {
            //   if (char != null) {
            //     console.log('rts ' + ByteUtils.base64ToHex(char.value))
            //   } else {
            //     console.log('null rts ' + error.message)
            //   }
            // })

            // infoChar.read().then((char) => {
            //   if (char != null) {
            //     console.log('info ' + ByteUtils.base64ToHex(char.value))
            //   } else {
            //     console.log('null info ' + error.message)
            //   }
            // })

            modeChar.writeWithResponse(ByteUtils.hexToBase64(ByteUtils.arrayToHex([BRSP_MODE_DATA]))).then((mode) => {
                if (mode != null) {
                    progressCallback(0.01, null)
                    if (mode.value != null) {
                        console.log("Write success, mode: " + ByteUtils.base64ToHex(mode.value))
                    } else {
                        console.log("Write success without a response value!")
                    }
                    this.sup.sendCommand(CommandReceiveChallenge)
                } else {
                    console.log("Write error!")
                    this.subscription.remove()
                    reject("Write error!")
                }
            })
        })
    }

    resetTimeout(reject, timeoutSec) {
        clearTimeout(this.timerId)
        this.timerId = setTimeout(() => {
            this.subscription.remove()
            reject("Measurement timeout!")
        }, + timeoutSec * 1000)
    }

}