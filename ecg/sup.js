import { ByteUtils } from "../byte-utils";
import xxtea from "./xxtea";

const CommandSetParameter = 0x0001;		/* Set configuration parameter */
const CommandGetParameter = 0x0002;		/* Get configuration parameter by parameter name */
const CommandStoreConfiguration = 0x0003;		/* Store all configuration parameters to the device */
const CommandGetNumberOfParameters = 0x0004;       /* Get number of configuration parameters */
const CommandGetParameterByIndex = 0x0005;		/* Get configuration parameter by index */
const CommandGetProtocolVersion = 0x0010;		/* Get configuration parameter by index */

const CommandGetBulkChunkSize = 0x0050;		/* Get the size of a bulk data chunk */

const CommandGetNumberOfEvents = 0x0101;		/* Get number of Events stored to the device */
const CommandGetEventHeaderItem = 0x0102;		/* Get value for certain item of the Event's Header */
const CommandEventStartTransfer = 0x0103;		/* Tell the device to start bulk transfer of Event's data */
const CommandEventStopTransfer = 0x0104;		/* Tell the device to stop bulk transfer of Event's data */
const CommandEraseAllEvents = 0x0105;		/* Erase all events */
const CommandRecordEvent = 0x0106;		/* Record event on the device */
const CommandCancelRecordingEvent = 0x0107;		/* Cancel recording event on the device, if one in progress */
const CommandMarkEventAsSent = 0x0108;		/* Mark Event as sent */
const CommandGetNumberOfUnsentEvents = 0x0109;		/* Get number of Events stored to the device */
const CommandGetEventParameters = 0x010A;    /* Get event params */
const CommandGetEventECGData = 0x010B;          /* Get a specific section of event data */

const CommandGetAnnotationSize = 0x0151;		/* Get Annotations' size */
const CommandAnnotationStartTransfer = 0x0152;     /* Tell the device to start bulk transfer of Event's annotation data */
const CommandAnnotationStopTransfer = 0x0153;     /* Tell the device to stop bulk transfer of Event's annotation data */
const CommandMctAnnotationStartTransfer = 0x0155;    /* Tell the device to start bulk transfer of Event or full disclosure annotation data */
const CommandMctAnnotationStopTransfer = 0x156;      /* Tell the device to stop bulk transfer of Event or full disclosure annotation data */

const CommandGetHolterSize = 0x0201;		/* Get Holter recording length */
const CommandHolterStartTransfer = 0x0202;		/* Tell the device to start bulk transfer of Holter data */
const CommandHolterStopTransfer = 0x0203;		/* Tell the device to stop bulk transfer of Holter data */
const CommandEraseHolterData = 0x0204;		/* Erase Holter data */
const CommandGetHolterECGSection = 0x0205;      /* Request a specific section of holter ECG data */
const CommandGetHolterAnnotationInfo = 0x0206;   /* Request the memory location of specified annotation data */
const CommandGetHolterAnnotationSection = 0x0207;    /* Request a specific section of Holter Annotation data */

const CommandStartLiveEcgTransfer = 0x0301;		/* Start bulk transfer of live ECG data */
const CommandStopLiveEcgTransfer = 0x0302;		/* Stop bulk transfer of live ECG data */

const CommandFirmwareUploadStart = 0x0401;		/* Upload firmware start */
const CommandFirmwareUploadChunk = 0x0402;		/* Upload firmware chunk */
const CommandFirmwareApply = 0x0403;		/* Apply uploaded firmware */

const CommandGetBatteryVoltage = 0x0501;		/* Get Battery voltage in milli volts from the device */
const CommandPing = 0x0502;		/* Ping command */
const CommandStartProcedure = 0x0503;		/* Tell the device to start the Procedure */
const CommandStopProcedure = 0x0504;		/* Tell the device to stop the Procedure */
const CommandStartStatusStream = 0x0505;		/* Tell the device to start sending status packets */
const CommandStopStatusStream = 0x0506;		/* Tell the device to stop sending status packets */
const CommandSoftReset = 0x0507;		/* Resets the device (erases recorded Event and Holter data and sets all config parameters to default values) */
const CommandSetDeviceTime = 0x0508;		/* Send system time to the device */
const CommandBlueToothShutdown = 0x0509;		/* Send command to PWM to shutdown Bluetooth connection */

const CommandSendChallenge = 0x050A;		/* Send decrypted data back to PWM */
export const CommandReceiveChallenge = 0x050B;		/* ASK PWM to send encrypted data */
const CommandGetStatus = 0x050C;         /* Get device status */
const CommandGetSerial = 0x050F;         /* Get device serial */

const PARAM_SAMPLE_RATE = "SAMP_RATE\0";
const PARAM_CABLE_ID = "CABLE_ID\0";
const PARAM_LAST_CABLE = "LAST_CABLE\0";

const BRSP_MODE_IDLE = 0x00;
const BRSP_MODE_DATA = 0x01;
const BRSP_MODE_COMMAND = 0x02;
const BRSP_MODE_FIRMWARE_UPDATE = 0x04;

const START_BYTE_1 = 0xEA;
const START_BYTE_2 = 0xA5;
const START_BYTES_IN_HEX = "eaa5";

const NETWORK_PACKET_HEADER_SIZE = 0x0006;
const NETWORK_PACKET_CHECKSUM_SIZE = 0x0002;

const SIRONA_RESPONSE_OK = 0xF000;
const SIRONA_RESPONSE_ERROR = 0xFF00;

const DATA_PACKET_TYPE = 0x00;
const STREAMING_DATA_PACKET_TYPE = 0x01;

const DATA_END_HEADER = "0000ffffffff"

// Dwonload the last 20 KiB of data
const HOLTER_DOWNLOAD_SIZE = 20 * 1024;

const ECG_VALUE_BITMASK = 0x3FF;

/**
 * Sirona Universal Protocol
 */
export class SUP {

    constructor(decodeKey, writeChar) {
        this.decodeKey = decodeKey
        this.writeChar = writeChar
    }

    sequenceNumber = 0
    recieveBuffer = new String('')

    createCommandString(command, param1 = '', param2 = '', param3 = '') {
        let data = START_BYTES_IN_HEX

        let type = '00' // data packet type
        data += type

        if (typeof this.sequenceNumber === 'undefined' || this.sequenceNumber >= 0x100) { this.sequenceNumber = 0 }
        data += ByteUtils.arrayToHex([+this.sequenceNumber % 0x100])
        this.sequenceNumber = +this.sequenceNumber + +1

        let len = (4 + param1.length + param2.length + param3.length) / 2
        data += ByteUtils.numberTo2ByteHex(len)

        data += ByteUtils.numberTo2ByteHex(command)
        data += param1
        data += param2
        data += param3

        let checksum = ByteUtils.byteArrayChecksum(ByteUtils.hexToArray(data))
        data += ByteUtils.numberTo2ByteHex(checksum)

        data = data.toLowerCase()
        console.log("packet: " + data)
        return ByteUtils.hexToBase64(data)
    }

    /**
     * @param {Number} command Command code
     * @param {String} param1 hex string
     * @param {String} param2 hex string
     * @param {String} param3 hex string
     */
    async sendCommand(command, param1 = '', param2 = '', param3 = '') {
        if (this.writeChar == null) {
            throw "No connection"
        }

        let packet = this.createCommandString(command, param1, param2, param3)

        // break the packet down to smaller pieces
        const step = 20
        for (let i = 0; i < packet.length; i += step) {
            let part = packet.slice(i, Math.min((i + step), packet.length))
            let char = await this.writeChar.writeWithResponse(part).catch((reject) => {
                console.log("Write error: " + reject.message)
                return
            })
            if (typeof char != 'undefined' && char != null && char.value != null) {
                console.log('Write success: ' + ByteUtils.base64ToHex(char.value))
            }
        }

    }

    parseRecievedData(resolve, reject, recieved, progressCallback) {
        this.recieveBuffer += recieved

        let start = this.recieveBuffer.lastIndexOf(START_BYTES_IN_HEX)
        let next = null
        let hex = null
        if (start == -1) {
            // no packet start
            console.log("No packet start: " + this.recieveBuffer)
            return
        } else if (start >= 1) {
            // end of last packet
            let last = this.recieveBuffer.substring(0, start)
            // new incoming packet part
            next = this.recieveBuffer.substring(start, this.recieveBuffer.length)
            // process last packet
            hex = last
        } else {
            hex = this.recieveBuffer
        }

        if (hex != null && hex.length >= (NETWORK_PACKET_HEADER_SIZE * 2)) {
            const len = ByteUtils.getIntFromHex(hex, 4, 6, true)
            const compeleteHexLen = (NETWORK_PACKET_HEADER_SIZE + len + NETWORK_PACKET_CHECKSUM_SIZE) * 2
            const type = ByteUtils.getIntFromHex(hex, 2, 3)
            if (len > 2048) {
                this.recieveBuffer = ""
                reject("Too large decoded size: " + len)
                return
            }

            if (hex.length > compeleteHexLen) {
                // Longer than expected, try to cut to size
                next = hex.slice(compeleteHexLen, hex.length)
                hex = hex.slice(0, compeleteHexLen)
            }

            if (hex.length == compeleteHexLen) {
                // complete packet, check it
                let calculatedChecksum = ByteUtils.byteArrayChecksum(ByteUtils.hexToArray(hex.slice(0, hex.length - 4)))
                let recievedChecksum = ByteUtils.getIntFromHex(hex.slice(hex.length - 4, hex.length), 0, 2, true)
                console.log("complete: " + hex.length + " checksums: " + calculatedChecksum + " " + recievedChecksum)

                if (calculatedChecksum != recievedChecksum) {
                    reject("Checksum error: " + hex)
                    this.recieveBuffer = ""
                    return
                }
                let command = ByteUtils.getIntFromHex(hex, 6, 8, true)
                let data = hex.slice(0 + (NETWORK_PACKET_HEADER_SIZE * 2), hex.length - (NETWORK_PACKET_CHECKSUM_SIZE * 2))

                this.recieveBuffer = ""
                if (type == DATA_PACKET_TYPE) {
                    // process commands snyc
                    this.parseRecievedCommand(resolve, reject, command, data)
                } else if (type == STREAMING_DATA_PACKET_TYPE) {
                    // process data async
                    this.parseStreamData(resolve, reject, command, data, progressCallback)
                } else {
                    reject("Unknown packet type: " + type)
                }
            } else if (hex.length < compeleteHexLen) {
                // incomong packet
                console.log("incomong packet: " + hex.length + "/" + compeleteHexLen)
            } else {
                // Still longer than expected!
                reject("Wrong packet size: " + hex.len)
            }
        } else {
            console.log("No length in packet: " + hex)
        }

        if (next != null) {
            // process new packet part
            this.parseRecievedData(resolve, reject, next)
        }
    }

    parseRecievedCommand(resolve, reject, command, data) {
        if (data == null || data.length < 8) {
            console.log("Invalid data for command: " + command)
            return
        }

        let respCode = ByteUtils.getIntFromHex(data, 2, 4, true)
        // handle errors
        if (respCode != SIRONA_RESPONSE_OK) {
            // TODO decode error codes
            let err = "Response error code: " + (respCode - SIRONA_RESPONSE_ERROR)
            console.log(err)
            reject(err)
            return
        }

        switch (command) {
            case CommandReceiveChallenge:
                console.log("CommandReceiveChallenge: " + data)
                this.answerChallenge(data);
                break;
            case CommandSendChallenge:
                console.log("CommandSendChallenge ACK")
                this.getDeviceStatus();
                break;
            case CommandGetStatus:
                console.log("CommandGetStatus: " + data)
                this.parseDeviceStatus(data);
                this.getChunkSize();
                break;
            case CommandGetBulkChunkSize:
                console.log("CommandGetBulkChunkSize: " + data)
                this.parseChunkSize(data);
                this.getParameter(PARAM_SAMPLE_RATE)
                break
            case CommandGetParameter:
                console.log("CommandGetParameter: " + data)
                this.parseSampleRate(data)
                this.getNumberOfEvents();
                break
            case CommandGetNumberOfEvents:
                console.log("CommandGetNumberOfEvents: " + data)
                this.parseNumberOfEvents(data)
                if (this.numberOfEvents >= 1) {
                    this.getLastEventParams();
                } else {
                    console.log("No evets to download!")
                    this.getHolterSize();
                }
                break
            case CommandGetEventParameters:
                console.log("CommandGetEventParameters: " + data)
                this.parseEventParams(data);
                this.startEventTransfer();
                break
            case CommandEventStartTransfer:
                console.log("CommandEventStartTransfer ACK")
                break
            case CommandEventStopTransfer:
                console.log("CommandEventStopTransfer ACK")
                break
            case CommandGetHolterSize:
                console.log("CommandGetHolterSize: " + data)
                this.parseHolterSize(data);
                if (this.holterSize > 0) {
                    this.startHolterTransfer()
                } else {
                    console.log("No holter data to download!")
                    reject("Nothing to download!")
                }
                break
            case CommandHolterStartTransfer:
                console.log("CommandHolterStartTransfer ACK")
                break
            case CommandHolterStopTransfer:
                console.log("CommandHolterStopTransfer ACK")
                break
            default:
                console.log("Unknown command: " + command)
                reject("Unknown command: " + command)
                break;
        }

    }

    parseDeviceStatus(data) {
        this.procedureType = ByteUtils.getIntFromHex(data, 4, 5);
        this.procedureState = ByteUtils.getIntFromHex(data, 5, 6);
        this.cableLeads = ByteUtils.getIntFromHex(data, 6, 7);
        this.cableChannels = ByteUtils.getIntFromHex(data, 7, 8);
        this.cableStatus = ByteUtils.getIntFromHex(data, 8, 9);
    }

    getDeviceStatus() {
        this.sendCommand(CommandGetStatus)
    }

    parseSampleRate(data) {
        let paramLength = ByteUtils.getIntFromHex(data, 4, 6, true);
        this.sampleRate = ByteUtils.getIntFromHex(data, 6, 6 + paramLength, true);
    }

    getParameter(parameter) {
        this.sendCommand(
            CommandGetParameter,
            ByteUtils.numberTo2ByteHex(parameter.length),
            ByteUtils.stringToHex(parameter)
        )
    }

    startHolterTransfer() {
        this.values = [];
        this.parsedSize = 0;
        let firstChunkNumber = Math.max(Math.floor((this.holterSize - HOLTER_DOWNLOAD_SIZE) / this.bulkChunkSize), 0);
        let lastChunkNumber = Math.floor(this.holterSize / this.bulkChunkSize);
        this.sendCommand(
            CommandHolterStartTransfer,
            ByteUtils.numberTo4ByteHex(firstChunkNumber),
            ByteUtils.numberTo4ByteHex(lastChunkNumber + 1)
        );
    }

    parseHolterSize(data) {
        this.holterSize = ByteUtils.getIntFromHex(data, 4, 8, true);
    }

    getHolterSize() {
        this.sendCommand(CommandGetHolterSize)
    }

    startEventTransfer() {
        this.values = [];
        this.parsedSize = 0;
        let lastChunkNumber = Math.floor(this.fullSize / this.bulkChunkSize);
        this.sendCommand(
            CommandEventStartTransfer,
            ByteUtils.numberTo2ByteHex(this.lastEvent),
            ByteUtils.numberTo4ByteHex(0),
            ByteUtils.numberTo4ByteHex(lastChunkNumber + 1)
        );
    }

    parseChunkSize(data) {
        this.bulkChunkSize = ByteUtils.getIntFromHex(data, 4, 6, true);
    }

    getChunkSize() {
        this.sendCommand(CommandGetBulkChunkSize);
    }

    parseEventParams(data) {
        this.fullSize = ByteUtils.getIntFromHex(data, 9, 13, true)
        if (this.cableChannels == 0xff) {
            // revert to 1 channel if we recieved dummy data
            this.cableChannels = 1
        }
    }

    getLastEventParams() {
        this.lastEvent = this.numberOfEvents - 1 // indexed from 0 for EventStartTransfer
        this.sendCommand(CommandGetEventParameters, ByteUtils.numberTo2ByteHex(this.lastEvent + 1)) // indexed from 1 for GetEventParameters
    }

    parseNumberOfEvents(data) {
        this.numberOfEvents = ByteUtils.getIntFromHex(data, 4, 6, true)
    }

    getNumberOfEvents() {
        this.sendCommand(CommandGetNumberOfEvents);
    }

    answerChallenge(data) {
        let challenge = ByteUtils.hexToBase64(data.slice(6 * 2, data.length));
        let key = ByteUtils.hexToArray(this.decodeKey);
        // console.log("challenge: " + challenge + " key: " + key)
        let decrypted = xxtea.decrypt(challenge, key);
        // console.log("decrypted: " + decrypted)
        this.sendCommand(CommandSendChallenge, ByteUtils.numberTo2ByteHex(decrypted.length), ByteUtils.arrayToHex(decrypted));
    }

    async parseStreamData(resolve, reject, command, data, progressCallback) {
        if (data == null || data.length < 8) {
            console.log("Invalid stream data for command: " + command)
            return
        }

        let respCode = ByteUtils.getIntFromHex(data, 2, 4, true)
        // handle errors
        if (respCode != SIRONA_RESPONSE_OK) {
            // TODO decode error codes
            let err = "Response error code: " + (respCode - SIRONA_RESPONSE_ERROR)
            console.log(err)
            reject(err)
            return false
        }

        let header = data.slice(8, 20)
        let sampleRate = this.sampleRate

        switch (command) {
            case CommandEventStartTransfer:
                console.log("CommandEventStartTransfer: " + data)
                if (header === DATA_END_HEADER) {
                    let ecgResult = this.createResultObject(sampleRate);
                    this.values = []
                    this.sendCommand(CommandEventStopTransfer);
                    resolve(ecgResult)
                } else {
                    let measurement = data.slice(20, data.length)
                    this.parseValues(measurement)
                    this.parsedSize += measurement.length / 2
                    let ecgResult = this.createResultObject(sampleRate);
                    progressCallback(this.parsedSize / this.fullSize, ecgResult)
                }
                break;
            case CommandHolterStartTransfer:
                console.log("CommandHolterStartTransfer: " + data)
                if (header === DATA_END_HEADER) {
                    let ecgResult = this.createResultObject(sampleRate);
                    this.values = []
                    this.sendCommand(CommandHolterStopTransfer);
                    resolve(ecgResult)
                } else {
                    let measurement = data.slice(20, data.length)
                    this.parseValues(measurement)
                    this.parsedSize += measurement.length / 2
                    let ecgResult = this.createResultObject(sampleRate);
                    progressCallback(this.parsedSize / HOLTER_DOWNLOAD_SIZE, ecgResult)
                }
                break;
            default:
                console.log("Unknown command: " + command)
                reject("Unknown command: " + command)
                return false;
        }
        return true;
    }

    createResultObject(SampleRate) {
        const SampleInterval = 1.0 / SampleRate

        let ecgResult = {};
        let len = this.fullSize * 0.75 * SampleInterval;
        let channels = this.cableChannels;
        let data = [];
        for (let i = 0; i < this.values.length; i += channels) {
            let dataPoint = {};
            dataPoint.time = SampleInterval * i;
            dataPoint.channels = [];
            dataPoint.channels.push(this.values[i]);
            if (channels > 1 && (i + 1) < this.values.length)
                dataPoint.channels.push(this.values[i + 1]);
            if (channels > 2 && (i + 2) < this.values.length)
                dataPoint.channels.push(this.values[i + 2]);
            data.push(dataPoint);
        }
        ecgResult.sampleRate = SampleRate;
        ecgResult.length = len;
        ecgResult.channels = channels;
        ecgResult.data = data;
        return ecgResult;
    }

    parseValues(measurementData) {
        if (measurementData == null) {
            console.log("No data to parse!")
            return
        }

        for (let i = 0; i < (measurementData.length / 8); i++) {
            let val = ByteUtils.getIntFromHex(measurementData, i * 4, (i + 1) * 4, true)

            let val1 = (val & ECG_VALUE_BITMASK);
            let val2 = ((val >> 10) & ECG_VALUE_BITMASK);
            let val3 = ((val >> 20) & ECG_VALUE_BITMASK);

            if (val1 >= 512) {
                val1 -= 1024;
            }
            if (val2 >= 512) {
                val2 -= 1024;
            }
            if (val3 >= 512) {
                val3 -= 1024;
            }
            this.values.push(val1);
            this.values.push(val2);
            this.values.push(val3);
        }
    }

}